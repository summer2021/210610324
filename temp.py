from mindspore.nn.dynamic_lr import piecewise_constant_lr
from src.utils.tools import get_lr
from src.model_utils.config import config as arg
def test_dynamic_lr():
    milestone = [2, 5, 10]
    learning_rates = [0.1, 0.05, 0.01]
    lr = piecewise_constant_lr(milestone, learning_rates)
    print(lr)

def test_lr():
    lrs = get_lr(arg)
    print(lrs)
    print(len(lrs),type(lrs))
if __name__ == '__main__':
    # test_dynamic_lr()
    test_lr()
