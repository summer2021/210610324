# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
######################## train cgan example ########################
train cgan according to model file:
python train.py --dataset_path ./data --real_valued_mnist True --net_type cnn --device_target CPU --epoch_size 10
python train.py --dataset_path ./data --real_valued_mnist True --net_type mlp --learning_rate 0.0002 --device_target CPU --epoch_size 100
python train.py --dataset_path ./data --real_valued_mnist True --net_type mlp --learning_rate 0.0002 --device_target GPU --epoch_size 200 --batch_size 128
"""
import numpy as np
import mindspore.nn as nn
from mindspore import context
from mindspore.communication.management import init, get_rank, get_group_size
from mindspore.common import set_seed
from mindspore import Tensor
from mindspore.common import dtype as mstype
from src.model_utils.config import config
from src.model_utils.moxing_adapter import moxing_wrapper
from src.dataset import get_dataset
from src.cgan import get_G_and_D,TrainOneStepG,TrainOneStepD,init_weights
from src.utils.tools import get_lr, load_ckpt
from src.utils.reporter import Reporter
from mindspore.nn.dynamic_lr import exponential_decay_lr
from src.losses import GANLoss,GeneratorLoss,DiscriminatorLoss
set_seed(1)

def modelarts_pre_process():
    pass

@moxing_wrapper(pre_process=modelarts_pre_process)
def train_lenet():

    if config.device_target == "CPU":
        config.run_distribute = False
        context.set_context(mode=context.PYNATIVE_MODE, device_target="CPU") # GRAPH_MODE #

    if config.run_distribute:
        if config.device_target == "Ascend":
            print('not done, please run on GPU')
            # device_id = int(os.getenv('DEVICE_ID'))
            # context.set_context(device_id=device_id, enable_auto_mixed_precision=True)
            # context.set_auto_parallel_context(device_num=args_opt.device_num, parallel_mode=ParallelMode.DATA_PARALLEL,
            #                                   gradients_mean=True)
            # set_algo_parameters(elementwise_op_strategy_follow=True)
            # init()
        # GPU target
        elif config.device_target == "GPU":
            context.set_context(mode=context.GRAPH_MODE, device_target="GPU")
            if config.isParallel:
                init("nccl")
                context.set_auto_parallel_context(parallel_mode=context.ParallelMode.DATA_PARALLEL,
                                                  device_num=get_group_size(), gradients_mean=True)#global_rank自动获取

        else:
            print('device_target error')
        init()
        config.rank_id = get_rank()
        config.rank_size = get_group_size()


    print('rank_id,device_num',config.rank_id,config.rank_size)
    ds_train = get_dataset(usage='train')
    data_loader = ds_train.create_dict_iterator()
    config.dataset_size = ds_train.get_dataset_size()
    print('data_loader init, rank_id:', config.rank_id, 'rank_size:', config.rank_size, 'len', ds_train.get_dataset_size())


    G, D = get_G_and_D(config)
    init_weights(G, config.init_type, config.init_gain)
    init_weights(D, config.init_type, config.init_gain)

    if config.load_ckpt:
        load_ckpt(config, G,D)

    loss = GANLoss(config.gan_loss)
    G_with_loss = GeneratorLoss(config, G, D, loss)
    D_with_loss = DiscriminatorLoss(config, G, D, loss)
    lrs = get_lr(config)
    print(lrs)
    print('datasize',config.dataset_size,len(lrs))
    optimizer_G = nn.Adam(G.trainable_params(), lrs, beta1=0.5)#
    optimizer_D = nn.Adam(D.trainable_params(), lrs, beta1=0.5)#
    net_G_train = TrainOneStepG(G_with_loss, optimizer_G)
    net_D_train = TrainOneStepD(D_with_loss, optimizer_D)

    reporter = Reporter(config)
    reporter.info('==========start training===============')
    fixed_noise = Tensor(np.random.normal(size=(config.batch_size, config.noise_dim)),
                   dtype=mstype.float32)
    fixed_label = Tensor((np.arange(fixed_noise.shape[0])%10), dtype=mstype.int32)
    # iterations
    i, j = 2, 1
    for epoch in range(config.start_epochs,config.epoch_size):
        reporter.epoch_start()
        for data in data_loader:
            real_img = data['image']
            label = data['label']
            noise = Tensor(np.random.normal(size=(config.batch_size, config.noise_dim)),
                                 dtype=mstype.float32)
            res_G, res_D = None, None
            for _ in range(j):
                res_D = net_D_train(real_img, noise, label)

            for _ in range(i):
                fake_img, res_G = net_G_train(noise, label)

            fixed_fake_img = G(fixed_noise,fixed_label)
            reporter.step_end(res_G, res_D)
            reporter.visualizer(fake_img,fixed_fake_img)
        reporter.epoch_end(net_G_train)
        j = 0 if (reporter.mean_loss_D*100 < reporter.mean_loss_G) else 1





    reporter.info('==========end training===============')

if __name__ == "__main__":
    train_lenet()
