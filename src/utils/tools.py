# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""Utils modified from cyclegan."""

import random
import numpy as np
from PIL import Image
from mindspore import Tensor
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from src.model_utils.config import config

class ImagePool():
    """
    This class implements an image buffer that stores previously generated images.
    This buffer enables us to update discriminators using a history of generated images
    rather than the ones produced by the latest generators.
    """
    def __init__(self, pool_size):
        """
        Initialize the ImagePool class
        Args:
            pool_size (int): the size of image buffer, if pool_size=0, no buffer will be created.
        """
        self.pool_size = pool_size
        if self.pool_size > 0:  # create an empty pool
            self.num_imgs = 0
            self.images = []

    def query(self, images):
        """
        Return an image from the pool.
        Args:
            images: the latest generated images from the generator
        Returns images Tensor from the buffer.
        By 50/100, the buffer will return input images.
        By 50/100, the buffer will return images previously stored in the buffer,
        and insert the current images to the buffer.
        """
        if isinstance(images, Tensor):
            images = images.asnumpy()
        if self.pool_size == 0:  # if the buffer size is 0, do nothing
            return Tensor(images)
        return_images = []
        for image in images:
            if self.num_imgs < self.pool_size:   # if the buffer is not full; keep inserting current images to the buffer
                self.num_imgs = self.num_imgs + 1
                self.images.append(image)
                return_images.append(image)
            else:
                p = random.uniform(0, 1)
                if p > 0.5:  # by 50% chance, the buffer will return a previously stored image, and insert the current image into the buffer
                    random_id = random.randint(0, self.pool_size - 1)  # randint is inclusive
                    tmp = self.images[random_id].copy()
                    self.images[random_id] = image
                    return_images.append(tmp)
                else:       # by another 50% chance, the buffer will return the current image
                    return_images.append(image)
        return_images = np.array(return_images)   # collect all the images and return
        if len(return_images.shape) != 4:
            raise ValueError("img should be 4d, but get shape {}".format(return_images.shape))
        return Tensor(return_images)


def save_image(img, img_path):
    """Save a numpy image to the disk
    Parameters:
        img (numpy array / Tensor): image to save.
        image_path (str): the path of the image.
    """
    if isinstance(img, Tensor):
        img = img.asnumpy()
        img = decode_image(img)
    elif isinstance(img, np.ndarray):
        img = decode_image(img)
    else:
        raise ValueError("img should be Tensor or numpy array, but get {}".format(type(img)))
    img_pil = Image.fromarray(img)#,mode='L'
    img_pil.save(img_path)


def decode_image(img,):
    """Decode a [B, C, H, W] Tensor to image numpy array."""
    img = img[:25]
    img = (img * 255).astype(np.uint8)
    # print('isinstance', img.shape, type(img))
    if config.net_type == 'mlp':
        img = img.reshape(25,1,28,28)
    img = np.concatenate(list(img), axis=1)#[C, H*B, W]
    img = np.split(img, 5, axis=1)   # list of [C, H*B/5, W]
    img = np.concatenate(img, axis=2)  # [C, H*5, W*5]
    img = img.transpose((1, 2, 0))

    if img.shape[-1]==1:
        img = np.concatenate((img,img,img), axis=2)
    return img


def get_lr(args):
    """
    Learning rate generator.
    For 'linear', we keep the same learning rate for the first <opt.n_epochs> epochs
    and linearly decay the rate to zero over the next <opt.n_epochs_decay> epochs.
    """
    if args.lr_policy == 'piecewise':
        lrs = [args.learning_rate] * args.dataset_size * args.piecewise_epochs
        lr_epoch = 0
        for epoch in range(args.n_epochs_decay):
            lr_epoch = args.learning_rate * (args.n_epochs_decay - epoch) / args.n_epochs_decay
            lrs += [lr_epoch] * args.dataset_size
        lrs += [lr_epoch] * args.dataset_size * (args.epoch_size - args.n_epochs_decay - args.n_epochs)
        return Tensor(np.array(lrs).astype(np.float32))
    if args.lr_policy == 'exponential':
        lr_epoch = args.learning_rate
        lrs = [lr_epoch] * args.dataset_size
        for epoch in range(args.n_epochs_decay - args.start_epochs - 1):
            lr_epoch = lr_epoch / args.decay_factor
            lrs += [lr_epoch] * args.dataset_size
        lrs += [lr_epoch] * args.dataset_size * (args.epoch_size - args.n_epochs_decay - args.start_epochs)
        lrs = lrs[args.start_epochs * args.dataset_size:]
        return Tensor(np.array(lrs).astype(np.float32))
    return args.learning_rate



def load_ckpt(args, G, D):
    """Load parameter from checkpoint."""
    if args.G_ckpt is not None:
        param_G = load_checkpoint(args.G_ckpt)
        load_param_into_net(G, param_G)
    if args.D_ckpt is not None:
        param_D = load_checkpoint(args.D_ckpt)
        load_param_into_net(D, param_D)
    print("load module from ",args.G_ckpt,' and ', args.D_ckpt)
