# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Produce the dataset
"""
import os, struct
import numpy as np
import mindspore.dataset as ds
import mindspore.dataset.vision.c_transforms as CV
import mindspore.dataset.transforms.c_transforms as C
from mindspore.dataset.vision import Inter
from mindspore.common import dtype as mstype
from src.model_utils.config import config
ds.config.set_num_parallel_workers(4)

def main():
    print(config.real_valued_mnist)
    # import mindspore
    # import mindspore.nn as nn
    # one = nn.OneHot(depth=10, axis=-1)
    # from mindspore.common.tensor import Tensor
    # indices = Tensor([1, 3], dtype=mindspore.int32)
    # print(one(indices).shape)
    # test numpy_read_dataset
    ds_train = get_real_valued_mnist(os.path.join(r'../data'), usage = 'all')
    print(ds_train.get_dataset_size())
    data_loader = ds_train.create_dict_iterator()
    data = next(data_loader)
    print(type(data['image']),data['image'].dtype,data['image'].shape)
    print(data['image'].asnumpy().min(),data['image'].asnumpy().max())
    print(type(data['label']), data['label'].dtype)
    print(data['image'].asnumpy())
    print(data['label'].asnumpy())

    # test ms_dataset
    # ds_train = ds.MnistDataset(os.path.join(r'../data', "train"), usage="all")#num_samples
    # data_loader = ds_train.create_dict_iterator()
    # print(ds_train.get_dataset_size())
    # data = next(data_loader)
    # print(type(data['image']),data['image'].dtype,data['image'].shape)
    # print(data['image'].asnumpy().min(),data['image'].asnumpy().max())
    # print(type(data['label']), data['label'].dtype)
    # # print(data['image'][0].asnumpy())
    # print(data['label'].asnumpy())
    # print(data['image'].asnumpy())

    # # test create_dataset_mlp
    # #
    ds_train = create_dataset_mlp(os.path.join(r'../data','train'),usage='train',
                              batch_size=config.batch_size, rank_id=config.rank_id, rank_size=config.rank_size)
    data_loader = ds_train.create_dict_iterator()
    data = next(data_loader)
    # print(type(data['image']),data['image'].dtype,data['image'].shape)
    # print(data['image'].asnumpy().min(),data['image'].asnumpy().max())
    # print(data['image'][0].asnumpy())
    print(data['label'].shape)
    print(data['label'].asnumpy())

    # # test create_dataset_cnn
    # config.real_valued_mnist = True
    # ds_train = create_dataset_cnn(os.path.join(r'../data'), usage='train',
    #                               batch_size=config.batch_size, rank_id=config.rank_id, rank_size=config.rank_size)
    # data_loader = ds_train.create_dict_iterator()
    # data = next(data_loader)
    # print(type(data['image']), data['image'].dtype, data['image'].shape)
    # print(data['image'].asnumpy().min(), data['image'].asnumpy().max())
    # print(data['image'][0,0,14].asnumpy())
    # print(data['label'][0,].asnumpy())


def get_dataset(usage='train'):
    if config.net_type == 'mlp':
        ds_train = create_dataset_mlp(os.path.join(config.dataset_path, usage), usage=usage, batch_size=config.batch_size,
                                      num_parallel_workers=config.num_parallel_workers, rank_id=config.rank_id, rank_size=config.rank_size)
    elif config.net_type == 'cnn':
        ds_train = create_dataset_cnn(os.path.join(config.dataset_path, usage), usage=usage, batch_size=config.batch_size,
                                      num_parallel_workers=config.num_parallel_workers,rank_id=config.rank_id, rank_size=config.rank_size)

    else:
        raise ValueError(f"Please set the correct network type to get the data: {config.net_type}")


    return ds_train

def create_dataset_mlp(data_path, usage='train', flatten_size=784, batch_size=32, repeat_size=1,
                   num_parallel_workers=1,rank_id=0,rank_size=1):
    """
    create dataset for train or test
    """
    # define dataset
    if config.real_valued_mnist:
        mnist_ds = get_real_valued_mnist(data_path, usage=usage, num_shards=rank_size, shard_id=rank_id)
    else:
        mnist_ds = ds.MnistDataset(os.path.join(data_path), num_shards=rank_size, shard_id=rank_id)

    # print('dataset init, rank_id/rank_size:',rank_id,'/',rank_size,mnist_ds.get_dataset_size())

    # define map operations
    rescale_op = CV.Rescale(1.0/255.0, 0)
    type_cast_op = C.TypeCast(mstype.int32)
    # onehot_op = C.OneHot(num_classes=10)

    # apply map operations on images
    # mnist_ds = mnist_ds.map(operations=onehot_op, input_columns="label",num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=type_cast_op, input_columns="label", num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=rescale_op, input_columns="image", num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=lambda x: (x.reshape((flatten_size,))), input_columns="image", num_parallel_workers=num_parallel_workers)

    # apply DatasetOps
    buffer_size = config.buffer_size
    # mnist_ds = mnist_ds.shuffle(buffer_size=buffer_size)  # 10000 as in LeNet train script
    mnist_ds = mnist_ds.batch(batch_size, drop_remainder=True)
    mnist_ds = mnist_ds.repeat(repeat_size)

    return mnist_ds


def create_dataset_cnn(data_path, usage='train', batch_size=32, repeat_size=1,
                   num_parallel_workers=1,rank_id=0,rank_size=1):
    """
    create dataset for train or test
    """
    # define dataset
    if config.real_valued_mnist:
        mnist_ds = get_real_valued_mnist(data_path, num_parallel_workers=num_parallel_workers, usage=usage, num_shards=rank_size, shard_id=rank_id)
    else:
        mnist_ds = ds.MnistDataset(data_path, num_parallel_workers=num_parallel_workers, num_shards=rank_size, shard_id=rank_id)

    # print('dataset init, rank_id/rank_size:',rank_id,'/',rank_size,mnist_ds.get_dataset_size())
    resize_height, resize_width = 32, 32

    # define map operations
    resize_op = CV.Resize((resize_height, resize_width), interpolation=Inter.LINEAR)
    rescale_op = CV.Rescale(1.0/255.0, 0)
    hwc2chw_op = CV.HWC2CHW()
    type_cast_op = C.TypeCast(mstype.int32)

    # apply map operations on images
    mnist_ds = mnist_ds.map(operations=type_cast_op, input_columns="label", num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=resize_op, input_columns="image", num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=rescale_op, input_columns="image", num_parallel_workers=num_parallel_workers)
    mnist_ds = mnist_ds.map(operations=hwc2chw_op, input_columns="image", num_parallel_workers=num_parallel_workers)

    # apply DatasetOps
    buffer_size = config.buffer_size

    mnist_ds = mnist_ds.shuffle(buffer_size=buffer_size)
    mnist_ds = mnist_ds.batch(batch_size, drop_remainder=True)
    mnist_ds = mnist_ds.repeat(repeat_size)

    return mnist_ds



def load_mnist(images_path, labels_path):
    """Load real valued MNIST data """
    with open(labels_path, 'rb') as lbpath:
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)
    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack('>IIII', imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 28, 28, 1)
    return images, labels


def get_real_valued_mnist(dataset_dir, usage = 'all', out_as_numpy=False, num_samples=None, num_parallel_workers=1, shuffle=None,
                          sampler=None, num_shards=None, shard_id=None):
    '''
    Load real valued MNIST dataset
    Args:
    dataset_dir (str): Path to the root directory that contains the dataset.
    usage (str, optional): Usage of this dataset, can be "train", "test" or "all" . "train" will read from 60,000
    train samples, "test" will read from 10,000 test samples, "all" will read from all 70,000 samples.
    (default=None, will read all samples)
    '''
    if usage == 'train':
        images_path = os.path.join(dataset_dir, f'train-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, f'train-labels.idx1-ubyte')
        images, labels = load_mnist(images_path, labels_path)
    elif usage == 'test':
        images_path = os.path.join(dataset_dir, f't10k-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, f't10k-labels.idx1-ubyte')
        images, labels = load_mnist(images_path, labels_path)
    elif usage == 'all':
        images_path = os.path.join(dataset_dir, 'train', f'train-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, 'train', f'train-labels.idx1-ubyte')
        images1, labels1 = load_mnist(images_path, labels_path)
        images_path = os.path.join(dataset_dir, 'test', f't10k-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, 'test', f't10k-labels.idx1-ubyte')
        images2, labels2 = load_mnist(images_path, labels_path)
        images, labels = np.concatenate([images1, images2],axis=0), np.concatenate([labels1, labels2],axis=0)
    else:
        raise ValueError(f"Unknow usage: {usage}")

    data = (images, labels)
    if out_as_numpy:
        return data
    dataset = ds.NumpySlicesDataset(data, column_names=["image", "label"], num_samples=num_samples, num_parallel_workers=num_parallel_workers,
                                    shuffle=shuffle, sampler=sampler,num_shards=num_shards, shard_id=shard_id)

    return dataset

if __name__ == '__main__':
    main()
