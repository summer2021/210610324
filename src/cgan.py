# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""cgan."""
import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.ops.operations as P
from mindspore.common import initializer as init
import mindspore.ops.functional as F
from mindspore.common.initializer import Normal
from mindspore import context
from mindspore.context import ParallelMode
from mindspore.parallel._auto_parallel_context import auto_parallel_context
from mindspore.communication.management import get_group_size



def init_weights(net, init_type='he', init_gain=0.02):
    """
    Initialize network weights.
    Parameters:
        net (Cell): Network to be initialized
        init_type (str): The name of an initialization method: normal | xavier.
        init_gain (float): Gain factor for normal and xavier.
    """
    for _, cell in net.cells_and_names():
        if isinstance(cell, (nn.Conv2d, nn.Conv2dTranspose)):
            if init_type == 'normal':
                cell.weight.set_data(init.initializer(init.Normal(init_gain), cell.weight.shape))
            elif init_type == 'xavier':
                cell.weight.set_data(init.initializer(init.XavierUniform(init_gain), cell.weight.shape))
            elif init_type == 'he':
                cell.weight.set_data(init.initializer(init.HeUniform(init_gain), cell.weight.shape))
            elif init_type == 'constant':
                cell.weight.set_data(init.initializer(0.001, cell.weight.shape))
            else:
                # print(init_type)
                raise NotImplementedError('initialization method [%s] is not implemented' % init_type)
        elif isinstance(cell, nn.BatchNorm2d):
            cell.gamma.set_data(init.initializer('ones', cell.gamma.shape))
            cell.beta.set_data(init.initializer('zeros', cell.beta.shape))
        elif isinstance(cell, nn.Dense):
            if init_type == 'normal':
                cell.weight.set_data(init.initializer(init.Normal(init_gain), cell.weight.shape))
            elif init_type == 'xavier':
                cell.weight.set_data(init.initializer(init.XavierUniform(init_gain), cell.weight.shape))
            elif init_type == 'he':
                cell.weight.set_data(init.initializer(init.HeUniform(init_gain), cell.weight.shape))
            elif init_type == 'constant':
                cell.weight.set_data(init.initializer(0.001, cell.weight.shape))
            cell.bias.set_data(init.initializer(0.001, cell.bias.shape))

def get_G_and_D(config):
    if config.net_type == 'mlp':
        G = Generator_mlp(config.noise_dim, config.mlp_out_size, auto_prefix=True)
        D = Discriminator_mlp(config.mlp_out_size, auto_prefix=True)
    elif config.net_type == 'cnn':
        G = Generater(noise_channel=100, img_channels=1, classes_num=10, embed_size=100, features_d=config.features_d,
                      norm_mode=config.g_norm_mode)
        D = Discriminator(img_channels=1, classes_num=10, embed_size=config.image_height * config.image_width,
                      features_d=config.features_d, img_size=32, norm_mode=config.d_norm_mode)
    else:
        print('Please set the correct network type,{} do not support.')

    return G, D

class Generater(nn.Cell):
    """
    Generater network
    """
    def __init__(self, noise_channel=100, img_channels=1,classes_num=10,embed_size=100, features_d=64, norm_mode=None, auto_prefix=True):
        super(Generater, self).__init__(auto_prefix=auto_prefix)
        self.concat = P.Concat(axis=1)
        self.embed = nn.Embedding(classes_num,embed_size)
        self.unsqueeze = P.ExpandDims()
        self.has_bias = True
        self.norm_mode = norm_mode
        if norm_mode is not None:
            self.has_bias = False
        self.net = nn.SequentialCell([
            self._block(noise_channel+embed_size, features_d*4, 4, 1, pad_mode='valid', padding=0),
            self._block(features_d * 4, features_d * 2, 4, 2, pad_mode='pad',padding=1),
            self._block(features_d * 2, features_d, 4, 2, pad_mode='pad', padding=1),
            nn.Conv2dTranspose(features_d, img_channels, 4, 2, pad_mode='pad', padding=1),
            nn.Sigmoid()
        ])


    def _block(self, in_channels, out_channels, kernel_size, stride, pad_mode, padding):
        return nn.SequentialCell([
            nn.Conv2dTranspose(in_channels, out_channels, kernel_size, stride, pad_mode, padding,has_bias=self.has_bias),
            nn.InstanceNorm2d(out_channels) if self.norm_mode=='instance' else nn.BatchNorm2d(out_channels),
            nn.ReLU()
        ])
    def construct(self, x,labels):

        embedding = self.embed(labels)
        x = self.concat((x,embedding))
        x = self.unsqueeze(self.unsqueeze(x,2),2)
        x = self.net(x)

        return x

class Discriminator(nn.Cell):
    def __init__(self, img_channels=1,classes_num=10,embed_size=32*32, features_d=64, img_size=32, norm_mode=None, auto_prefix=True):
        super(Discriminator, self).__init__(auto_prefix=auto_prefix)
        self.img_size = img_size
        self.has_bias = True
        self.norm_mode = norm_mode
        if norm_mode is not None:
            self.has_bias = False
        self.net =  nn.SequentialCell([
            self._block(img_channels+1, features_d, 4, 2, pad_mode='pad',padding=1),
            self._block(features_d, features_d * 2, 4, 2, pad_mode='pad',padding=1),
            self._block(features_d * 2, features_d * 4, 4, 2, pad_mode='pad',padding=1),
            nn.Conv2d(features_d * 4, 1, 4, 2, pad_mode='valid')
        ])
        self.concat = P.Concat(axis=1)
        self.embed = nn.Embedding(classes_num, embed_size)
        # P.Reshape()

    def _block(self, in_channels, out_channels, kernel_size, stride, pad_mode, padding):
        return nn.SequentialCell([
            nn.Conv2d(in_channels, out_channels, kernel_size, stride, pad_mode, padding,has_bias=self.has_bias),
            nn.InstanceNorm2d(out_channels) if self.norm_mode=='instance' else nn.BatchNorm2d(out_channels),
            nn.LeakyReLU()
        ])
    def construct(self, x, label):
        embedding = self.embed(label).reshape(label.shape[0], 1, self.img_size, self.img_size)
        x = self.concat((x, embedding))
        x = self.net(x)
        return x



class Generator_mlp(nn.Cell):
    def __init__(self, noise_dims, output_dim, auto_prefix=True):
        super().__init__(auto_prefix=auto_prefix)
        self.onehot = nn.OneHot(depth=10, axis=-1)
        self.fc1 = nn.Dense(noise_dims+10, 256)
        self.fc2 = nn.Dense(256, 512)
        self.fc3 = nn.Dense(512, output_dim)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()
        self.concat = P.Concat(1)

    def construct(self, x, label):
        label = self.onehot(label)
        x = self.concat((x, label))

        x = self.fc1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.sigmoid(x)
        return x


class Discriminator_mlp(nn.Cell):
    def __init__(self, input_dims, auto_prefix=True):
        super().__init__(auto_prefix=auto_prefix)
        self.onehot = nn.OneHot(depth=10, axis=-1)
        self.fc1 = nn.Dense(input_dims + 10, 256)
        self.fc2 = nn.Dense(256, 128)
        self.fc3 = nn.Dense(128, 1)
        self.lrelu = nn.LeakyReLU(0.01)
        self.concat = P.Concat(1)
        self.dropout = P.Dropout()
        self.sigmoid = nn.Sigmoid()

    def construct(self, x, label):
        label = self.onehot(label)
        x = self.concat((x, label))
        x = self.fc1(x)
        x = self.lrelu(x)
        x, _ = self.dropout(x)
        x = self.fc2(x)
        x = self.lrelu(x)
        x, _ = self.dropout(x)
        x = self.fc3(x)
        # x = self.sigmoid(x)
        return x



class WithLossCell(nn.Cell):
    """
    Wrap the network with loss function to return generator loss.
    Args:
        network (Cell): The target network to wrap.
    """
    def __init__(self, network):
        super(WithLossCell, self).__init__(auto_prefix=False)
        self.network = network

    def construct(self, noise, label):
        _, lg = self.network(noise, label)
        return lg

class TrainOneStepG(nn.Cell):
    """
    Encapsulation class of CGAN generator network training.
    Append an optimizer to the training network after that the construct
    function can be called to create the backward graph.
    Args:
        G (Cell): Generator with loss Cell. Note that loss function should have been added.
        generator (Cell): Generator of CycleGAN.
        optimizer (Optimizer): Optimizer for updating the weights.
        sens (Number): The adjust parameter. Default: 1.0.
    """
    def __init__(self, G_with_loss, optimizer, sens=1.0):
        super(TrainOneStepG, self).__init__(auto_prefix=False)
        self.optimizer = optimizer
        self.G_with_loss = G_with_loss
        self.G_with_loss.set_grad()
        self.G_with_loss.set_train()

        self.grad = ops.GradOperation(get_by_list=True, sens_param=True)
        self.sens = sens
        self.weights = optimizer.parameters
        self.loss_net = WithLossCell(G_with_loss)
        self.reducer_flag = False
        self.grad_reducer = None
        self.parallel_mode = context.get_auto_parallel_context("parallel_mode")
        if self.parallel_mode in [ParallelMode.DATA_PARALLEL, ParallelMode.HYBRID_PARALLEL]:
            self.reducer_flag = True
        if self.reducer_flag:
            mean = context.get_auto_parallel_context("gradients_mean")
            if auto_parallel_context().get_device_num_is_set():
                degree = context.get_auto_parallel_context("device_num")
            else:
                degree = get_group_size()
            self.grad_reducer = nn.DistributedGradReducer(optimizer.parameters, mean, degree)

    def construct(self, noise, label):
        fake_img,loss_G = self.G_with_loss(noise, label)
        sens = ops.Fill()(ops.DType()(loss_G), ops.Shape()(loss_G), self.sens)
        grads_g = self.grad(self.loss_net, self.weights)(noise, label, sens)
        if self.reducer_flag:
            # apply grad reducer on grads
            grads_g = self.grad_reducer(grads_g)

        return fake_img, ops.depend(loss_G, self.optimizer(grads_g))



class TrainOneStepD(nn.Cell):
    """
    Encapsulation class of CGAN discriminator network training.
    Append an optimizer to the training network after that the construct
    function can be called to create the backward graph.
    Args:
        D (Cell): D with loss Cell. Note that loss function should have been added.
        optimizer (Optimizer): Optimizer for updating the weights.
        sens (Number): The adjust parameter. Default: 1.0.
    """
    def __init__(self, D_with_loss, optimizer, sens=1.0):
        super(TrainOneStepD, self).__init__(auto_prefix=False)
        self.optimizer = optimizer
        self.D_with_loss = D_with_loss
        self.D_with_loss.set_grad()
        self.D_with_loss.set_train()
        self.grad = ops.GradOperation(get_by_list=True, sens_param=True)
        self.sens = sens
        self.weights = optimizer.parameters
        self.reducer_flag = False
        self.grad_reducer = None
        self.parallel_mode = context.get_auto_parallel_context("parallel_mode")
        if self.parallel_mode in [ParallelMode.DATA_PARALLEL, ParallelMode.HYBRID_PARALLEL]:
            self.reducer_flag = True
        if self.reducer_flag:
            mean = context.get_auto_parallel_context("gradients_mean")
            if auto_parallel_context().get_device_num_is_set():
                degree = context.get_auto_parallel_context("device_num")
            else:
                degree = get_group_size()
            self.grad_reducer = nn.DistributedGradReducer(optimizer.parameters, mean, degree)

    def construct(self, real_img, noise, label):
        # print(type(real_img),type(noise),type(label),)
        weights = self.weights
        ld = self.D_with_loss(real_img, noise, label)
        # print(ld.shape)
        sens_d = ops.Fill()(ops.DType()(ld), ops.Shape()(ld), self.sens)
        grads_d = self.grad(self.D_with_loss, weights)(real_img, noise, label, sens_d)
        if self.reducer_flag:
            # apply grad reducer on grads
            grads_d = self.grad_reducer(grads_d)
        return ops.depend(ld, self.optimizer(grads_d))

if __name__ == '__main__':
    import os
    import dataset
    from src.model_utils.config import config
    G = Generator_mlp(config.noise_dim, config.mlp_out_size, auto_prefix=True)
    D = Discriminator_mlp(config.mlp_out_size, auto_prefix=True)
    ds_train = dataset.create_dataset_mlp(os.path.join('../data', 'train'), usage='train', batch_size=config.batch_size,
                                  num_parallel_workers=config.num_parallel_workers, rank_id=config.rank_id,
                                  rank_size=config.rank_size)
    data_loader = ds_train.create_dict_iterator()
    data = next(data_loader)
    real_img = data['image']
    from mindspore.common.tensor import Tensor
    import numpy as np
    from mindspore.common import dtype as mstype
    noise = Tensor(np.random.normal(size=(config.batch_size, config.noise_dim)),
                   dtype=mstype.float32)
    label = data['label']
    print(real_img.shape,label.shape)
    # out = G(noise,label)
    out = D(real_img,label)
    print(out.shape)
