import os, sys,time,gc
import argparse
import mindspore.nn as nn
# import mindspore.numpy as np
import numpy as np
import matplotlib.pyplot as plt
import mindspore, numpy
from mindspore import Tensor
from src.dataset import create_dataset_mlp
from mindspore import context
from mindspore import dtype as mstype
from src.cgan import get_G_and_D
from src.utils.tools import load_ckpt
from src.dataset import get_real_valued_mnist
from src.model_utils.config import config
pi = Tensor(numpy.pi)

"""
python parzen.py --batch_size 100
"""

def test_code():
    ds_train = create_dataset_mlp(os.path.join(r'./data', 'train'), usage='train',
                                  batch_size=60000)
    data_loader = ds_train.create_dict_iterator()
    data = next(data_loader)
    samples = data['image'][:1000]
    print(samples.shape)
    # ds_train = create_dataset_mlp(os.path.join(r'./data', 'test'), usage='test',
    #                               batch_size=10000)
    # data_loader = ds_train.create_dict_iterator()
    # data = next(data_loader)
    valid = data['image'][50000:51000]
    test = valid
    print(valid.shape)
    # print(log_mean_exp(a))
    sigma_range = np.logspace(-1, 0, num=10)
    batch_size=100
    sigma, _ = cross_validate_sigma(samples, valid, sigma_range, batch_size)
    print('Using Sigma: {}'.format(sigma))
    gc.collect()

    # fit and evaulate
    parzen = parzen_estimation(samples, sigma)
    ll = get_nll(test, parzen, batch_size=batch_size)
    se = ll.std() / numpy.sqrt(test.shape[0])

    print("Log-Likelihood of test set = {}, se: {}".format(ll.mean(), se))

def main():
    context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
    batch_size = config.batch_size

    # load test set
    train_data = get_real_valued_mnist(dataset_dir=r'./data/test', usage='test', out_as_numpy=True)
    test_data = get_real_valued_mnist(dataset_dir=r'./data/test',usage='test',out_as_numpy=True)
    test_data = test_data[0]
    train_data = train_data[0]
    print(test_data.shape)
    print(test_data.max(),test_data.min())
    sample = Samples(config)
    sample_list = [sample.get_sample() for i in range(10)]#np.concatenate(list(img), axis=1)
    samples = np.concatenate(sample_list,axis=0)
    print(samples.shape,samples.max(),samples.min())
    """fs"""
    valid = train_data[50000:60000]  # 50000-60000
    # cross validate sigma
    if config.sigma is None:
        sigma_range = numpy.logspace(config.sigma_start, config.sigma_end, num=config.cross_val)
        sigma = cross_validate_sigma(samples, valid, sigma_range, batch_size)
    else:
        sigma = float(config.sigma)

    # evaluate
    parzen = parzen_estimation(samples, sigma)
    ll = get_nll(valid, parzen, batch_size=batch_size)
    se = ll.std() / numpy.sqrt(valid.shape[0])
    print("Log-Likelihood of test set = {}, se: {}".format(ll.mean(), se))



class Samples(nn.Cell):
    def __init__(self,args):
        super().__init__()
        # load model
        self.G, D = get_G_and_D(args)
        load_ckpt(args, self.G, D)
        self.batch_size = args.batch_size
        self.noise_dim =  args.noise_dim

    def get_sample(self):
        noise = Tensor(numpy.random.normal(size=(self.batch_size, self.noise_dim)),
                       dtype=mstype.float32)
        label = Tensor((np.arange(noise.shape[0])%10), dtype=mstype.int32)
        samples = self.G(noise, label).asnumpy()# 10000
        # print(samples.max())
        return samples

# 构造Parzen window估计函数
def parzen_estimation(mu, sigma, mode='gauss'):
    """
    Implementation of a parzen-window estimation
    Keyword arguments:
        x: A "nxd"-dimentional numpy array, which each sample is
                  stored in a separate row (=training example)
        mu: point x for density estimation, "dx1"-dimensional numpy array
        sigma: window width
    Return the density estimate p(x)
    """
    def log_mean_exp(a):
        # print(a.shape)
        max_ = a.max(axis=1)
        return max_ + np.log(np.exp(a - np.expand_dims(max_, axis=1)).mean(1))

    def gaussian_window(x, mu, sigma):
        # x:nxd ,mu:dx1
        # print(x.shape,mu.shape,sigma)
        a = (np.expand_dims(x, axis=1) - np.expand_dims(mu, axis=0)) / sigma
        # print(a.shape)# (100, 1000, 784)
        E = log_mean_exp(-0.5 * (a ** 2).sum(-1))
        Z = mu.shape[1] * np.log(sigma * np.sqrt(np.pi * 2))
        # print(mu.shape[-1])
        # print(np.log(sigma * np.sqrt(np.pi * 2)) > 0)
        # print((np.log(sigma)+ np.log(np.sqrt(np.pi * 2)) > 0))
        # print((E - Z) > 0)
        # return np.exp(E - Z)
        return E - Z

    def hypercube_kernel(x, mu, h):
        n, d = mu.shape

        a = (np.expand_dims(x, axis=1) - np.expand_dims(mu, axis=0)) / h
        b = np.all(np.less(np.abs(a), 1/2), axis=-1) #检查x1是否小于x2
        kn = np.sum(b.astype(int), axis=-1)
        return kn / (n * h**d) # 概率密度

    if mode is 'gauss':
        return lambda x: gaussian_window(x, mu, sigma)
    elif mode is 'hypercube':
        return lambda x: hypercube_kernel(x, mu, h=sigma)



# Pdf estimation
def pdf_multivaraible_gauss(x, mu, cov):
    # print('pdf',x.shape)
    part1 = 1 / ((2 * np.pi) ** (len(mu) / 2) * (np.linalg.det(cov) ** (1 / 2)))
    part2 = (-1 / 2) * (x - mu).T.dot(np.linalg.inv(cov)).dot((x - mu))
    # part2 = (-1 / 2) * (x - mu).dot(np.linalg.inv(cov)).dot((x - mu).T)
    # print(part2.shape)
    return part1 * np.exp(part2)


def get_nll(x, parzen, batch_size=10):
    """
    Credit: Yann N. Dauphin
    """

    inds = range(x.shape[0])
    n_batches = int(np.ceil(float(len(inds)) / batch_size))
    nlls = []
    for i in range(n_batches):
        nll = parzen(x[inds[i::n_batches]])
        nlls.extend(nll)
    return np.array(nlls)

def cross_validate_sigma(samples, data, sigmas, batch_size):

    lls = []
    for sigma in sigmas:
        parzen = parzen_estimation(samples, sigma,mode='gauss')
        ll = get_nll(data, parzen, batch_size = batch_size)
        lls.append(ll.mean())
        del parzen
        gc.collect()
        print('sigma:', sigma,'ll_m:',ll.mean())
    ind = np.argmax(lls)
    return sigmas[ind],lls
#
# # 构造Parzen window估计函数
# def parzen_estimation(mu, sigma, mode='gauss'):
#     """
#     Implementation of a parzen-window estimation
#     Keyword arguments:
#         x: A "nxd"-dimentional numpy array, which each sample is
#                   stored in a separate row (=training example)
#         mu: point x for density estimation, "dx1"-dimensional numpy array
#         sigma: window width
#     Return the density estimate p(x)
#     """
#     def log_mean_exp(a):
#         # print(a.shape)
#         max_ = np.amax(a,axis=1)
#         # np.mean()
#         return max_ + np.log(np.exp(a - np.expand_dims(max_, axis=1)).mean(1))
#
#     def gaussian_window(x, mu, sigma):
#         # x:nxd ,mu:dx1
#         # print(x.shape,mu.shape,sigma)
#         a = (np.expand_dims(x, axis=1) - np.expand_dims(mu, axis=0)) / sigma
#         # print(a.shape)# (100, 1000, 784)
#         # E = log_mean_exp(-0.5 * (a ** 2)).cumsum(-1)
#         E = np.nansum(log_mean_exp(-0.5 * (a ** 2)), axis=-1)
#         Z = mu.shape[1] * np.log(sigma * np.sqrt(pi * 2))
#         # print(mu.shape[-1])
#         # print(np.log(sigma * np.sqrt(np.pi * 2)) > 0)
#         # print((np.log(sigma)+ np.log(np.sqrt(np.pi * 2)) > 0))
#         # print(E.shape,Z.shape)
#         # return np.exp(E - Z)
#         return E - Z
#
#     return lambda x: gaussian_window(x, mu, sigma)
#
#
#
# # Pdf estimation
# def pdf_multivaraible_gauss(x, mu, cov):
#     # print('pdf',x.shape)
#     part1 = 1 / ((2 * np.pi) ** (len(mu) / 2) * (np.linalg.det(cov) ** (1 / 2)))
#     part2 = (-1 / 2) * (x - mu).T.dot(np.linalg.inv(cov)).dot((x - mu))
#     # part2 = (-1 / 2) * (x - mu).dot(np.linalg.inv(cov)).dot((x - mu).T)
#     # print(part2.shape)
#     return part1 * np.exp(part2)
#
#
# def get_nll(x, parzen, batch_size=10):
#     """
#     Credit: Yann N. Dauphin
#     """
#
#     inds = range(x.shape[0])
#     n_batches = np.ceil(float(len(inds)) / batch_size).astype(np.int32, copy=True)
#     nlls = []
#     for i in range(n_batches):
#         # print(inds[i::n_batches])
#         nll = parzen(x[list(inds[i::n_batches])])
#         nlls.append(nll)
#         # print(type(nll),nll.shape)
#         # print(type(nlls),len(nlls))
#         # nlls = np.concatenate((nlls, nll), axis=0)
#     return np.array(nlls).squeeze()
#
# def cross_validate_sigma(samples, data, sigmas, batch_size):
#
#     lls = []
#     for sigma in sigmas:
#         parzen = parzen_estimation(samples, sigma,mode='gauss')
#         ll = get_nll(data, parzen, batch_size = batch_size)
#         # print(ll.shape,type(ll.mean()))
#         lls.append(ll.mean())
#         del parzen
#         gc.collect()
#         print('sigma:', sigma,'ll_m:',ll.mean())
#     # lls = lls.asnumpy()
#     # print(len(lls))
#     lls = np.array(lls).squeeze()
#     # print(lls.shape)
#     ind = Tensor(numpy.argmax(lls))
#     # print(ind,type(ind))
#     return sigmas[ind],lls

if __name__ == '__main__':
    main()
    # test_code()