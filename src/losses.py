import numpy as np
import mindspore
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import Tensor
import mindspore.common.dtype as mstype
from mindspore.ops import constexpr


class SigmoidBCEWithLogits(nn.Cell):
    """
    SigmoidBCEWithLogits creates a criterion to measure the Binary Cross Entropy between the true labels and
    predicted labels with sigmoid logits.
    Args:
        reduction (str): Specifies the reduction to be applied to the output.
            Its value must be one of 'none', 'mean', 'sum'. Default: 'none'.
    Outputs:
        Tensor or Scalar, if `reduction` is 'none', then output is a tensor and has the same shape as `inputs`.
        Otherwise, the output is a scalar.
    """
    def __init__(self, reduction='mean'):
        super(SigmoidBCEWithLogits, self).__init__()
        if reduction is None:
            reduction = 'none'
        if reduction not in ('mean', 'sum', 'none'):
            raise ValueError(f"reduction method for {reduction.lower()} is not supported")
        self.loss = ops.SigmoidCrossEntropyWithLogits()

        self.reduce = False
        if reduction == 'sum':
            self.reduce_mode = ops.ReduceSum()
            self.reduce = True
        elif reduction == 'mean':
            self.reduce_mode = ops.ReduceMean()
            self.reduce = True
    def construct(self, predict, target):

        loss = self.loss(predict, target)
        if self.reduce:
            loss = self.reduce_mode(loss)
        return loss


class Reduction(nn.Cell):
    """
    SigmoidBCEWithLogits creates a criterion to measure the Binary Cross Entropy between the true labels and
    predicted labels with sigmoid logits.
    Args:
        reduction (str): Specifies the reduction to be applied to the output.
            Its value must be one of 'none', 'mean', 'sum'. Default: 'none'.
    Outputs:
        Tensor or Scalar, if `reduction` is 'none', then output is a tensor and has the same shape as `inputs`.
        Otherwise, the output is a scalar.
    """
    def __init__(self, reduction='mean'):
        super(Reduction, self).__init__()
        if reduction is None:
            reduction = 'none'
        if reduction not in ('mean', 'sum', 'none'):
            raise ValueError(f"reduction method for {reduction.lower()} is not supported")

        self.reduce = False
        if reduction == 'sum':
            self.reduce_mode = ops.ReduceSum()
            self.reduce = True
        elif reduction == 'mean':
            self.reduce_mode = ops.ReduceMean()
            self.reduce = True
    def construct(self, loss, target):
        if self.reduce:
            loss = self.reduce_mode(loss)
        return loss



class GANLoss(nn.Cell):
    """
    The GANLoss class abstracts away the need to create the target label tensor
    that has the same size as the input.
    Args:
        mode (str): The type of GAN objective. It currently supports 'vanilla', 'lsgan'. Default: 'lsgan'.
        reduction (str): Specifies the reduction to be applied to the output.
            Its value must be one of 'none', 'mean', 'sum'. Default: 'none'.
    Parameters:
            gan_loss (str) - - the type of GAN objective. It currently supports vanilla, lsgan, and wgangp.
            target_real_label (bool) - - label for a real image
            target_fake_label (bool) - - label of a fake image

        Note: Do not use sigmoid as the last layer of Discriminator.
        LSGAN needs no sigmoid. vanilla GANs will handle it with BCEWithLogitsLoss.
    Outputs:
        Tensor or Scalar, if `reduction` is 'none', then output is a tensor and has the same shape as `inputs`.
        Otherwise, the output is a scalar.
    """
    def __init__(self, mode="bce", reduction='mean'):
        super(GANLoss, self).__init__()
        self.loss = None
        self.ones = ops.OnesLike()
        # wgan wgangp
        if mode == "lsgan":
            self.loss = nn.MSELoss(reduction)
            # nn.SigmoidCrossEntropyWithLogits()
        elif mode == "bce":
            self.loss = SigmoidBCEWithLogits(reduction)
        elif mode =="wgangp":
            self.loss = None

        else:
            raise NotImplementedError(f'GANLoss {mode} not recognized, we support lsgan and vanilla.')

    def construct(self, predict, target):
        target = ops.cast(target, ops.dtype(predict))
        target = self.ones(predict) * target
        loss = self.loss(predict, target)
        return loss



class GeneratorLoss(nn.Cell):
    """
    CGAN generator loss.
    Args:
        args (class): Option class.
        generator (Cell): Generator of GAN.
        discriminator (Cell): Discriminator of GAN.
    Outputs:
        the losses of generator.
    """
    def __init__(self, args, generator, discriminator, loss):
        super(GeneratorLoss, self).__init__()

        self.dis_loss = loss #GANLoss(args.gan_mode)
        self.generator = generator
        self.discriminator = discriminator
        self.true = Tensor(True, mstype.bool_)
        self.gan_loss = args.gan_loss
        if self.gan_loss =='wgangp':
            self.reduce_mode = ops.ReduceMean()

    def construct(self, noise, label):
        fake_img  = self.generator(noise, label)
        d_out = self.discriminator(fake_img, label)
        if self.gan_loss == 'wgangp':
            return  (fake_img,-self.reduce_mode(d_out))
        loss_G = self.dis_loss(d_out, self.true)
        return (fake_img,loss_G)



@constexpr
def generate_tensor(batch_size, net_type):
    if net_type == 'cnn':
        np_array = np.random.randn(batch_size, 1, 1, 1)
    if net_type == 'mlp':
        np_array = np.random.randn(batch_size, 1)
    return mindspore.Tensor(np_array, mindspore.float32)



class WGANGPGradientPenalty(nn.Cell):
    def __init__(self, discrimator, lambdaGP=10, net_type='cnn'):
        super(WGANGPGradientPenalty, self).__init__()
        self.gradient_op = ops.GradOperation()
        self.reduce_sum = ops.ReduceSum()
        self.reduce_mean = ops.ReduceMean()
        self.sqrt = ops.Sqrt()
        self.discrimator = discrimator
        # self.gradientWithInput = GradientWithInput(discrimator)
        self.lambdaGP = mindspore.Tensor(lambdaGP, mindspore.float32)
        # self.gradient_function = self.gradient_op(self.gradientWithInput)
        self.net_type = net_type

    def construct(self, real, fake, label):
        # print(type(real), type(fake), type(label), )
        batch_size = real.shape[0]
        alpha = generate_tensor(batch_size,self.net_type)
        # print(alpha.shape,real.shape)
        alpha = alpha.expand_as(real)
        # print(alpha.shape)
        interpolates = alpha * real + ((1 - alpha) * fake)
        gradient = self.gradient_op(self.discrimator)(interpolates, label)
        gradient = ops.reshape(gradient, (batch_size, -1))
        # print(gradient.shape)
        gradient = self.sqrt(self.reduce_sum(gradient * gradient, 1))
        gradient_penalty = self.reduce_mean((gradient - 1.0) ** 2) * self.lambdaGP

        return gradient_penalty

class DiscriminatorLoss(nn.Cell):
    """
    CGAN generator loss.
    Args:
        args (class): Option class.
        generator (Cell): Generator of GAN.
        discriminator (Cell): Discriminator of GAN.
    Outputs:
        the losses of discriminator.
    """
    def __init__(self, args, generator, discriminator, loss):
        super(DiscriminatorLoss, self).__init__()

        self.dis_loss = loss #GANLoss(args.gan_mode)
        self.generator = generator
        self.discriminator = discriminator
        self.true = Tensor(True, mstype.bool_)
        self.false = Tensor(False, mstype.bool_)
        self.gan_loss = args.gan_loss
        if self.gan_loss == 'wgangp':
            self.reduce_mode = ops.ReduceMean()
            self.gradient_penalty = WGANGPGradientPenalty(self.discriminator, lambdaGP=10, net_type=args.net_type)

    def construct(self, real_img, noise, label):
        # print(real_img.shape, noise.shape, type(label))
        fake_img = self.generator(noise, label)
        fake_D = self.discriminator(fake_img, label)
        real_D = self.discriminator(real_img, label)
        if self.gan_loss == 'wgangp':
            # print(self.reduce_mode(real_D))
            print(self.gradient_penalty(real_img, fake_img, label).shape)
            return -(self.reduce_mode(real_D) - self.reduce_mode(fake_D)) + self.gradient_penalty(real_img, fake_img, label)

        loss_D_f = self.dis_loss(fake_D, self.false)
        loss_D_r = self.dis_loss(real_D, self.true)
        loss_D = (loss_D_f + loss_D_r) * 0.5
        return loss_D