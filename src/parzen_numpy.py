import sys,time,gc,struct,os
import numpy as np
import matplotlib.pyplot as plt
# from src.dataset import get_real_valued_mnist, load_mnist
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from multiprocessing import Pool
from functools import partial
#不同核宽度对pdf估计的影响

def main():
    # print(len(getdata(8)))
    # dataset = load_mnist()
    dataset = get_real_valued_mnist(dataset_dir=r'./../data/train', usage='train')
    dataset_test = get_real_valued_mnist(dataset_dir=r'./../data/test', usage='test')
    dataset = dataset[0]
    dataset_test = dataset_test[0]
    print(dataset.shape,dataset.dtype,type(dataset))
    valid_data = dataset[50000:51000].reshape(-1,784).astype('float')
    test_data = dataset_test.reshape(-1,784).astype('float')

    sample_data = dataset[:1000].reshape(-1,784).astype('float')

    valid_data = valid_data / 255
    test_data = test_data / 255

    sample_data = sample_data / 255

    print(sample_data.max(),sample_data.min())
    batch_size = 100
    # sigmas = np.linspace(1e-4, 1, 20)
    sigma_range = np.logspace(-1, 0, 10)
    sigma,lls = cross_validate_sigma(sample_data, valid_data, sigma_range, batch_size,10)
    print(sigma)
    parzen = parzen_estimation(sample_data, sigma, mode='gauss')

    ll = get_nll(test_data,parzen,batch_size=batch_size)
    se = ll.std() / np.sqrt(test_data.shape[0])
    print("Log-Likelihood of test set = {}, se: {}".format(ll.mean(), se))


def main2():
    # Make data
    np.random.seed(2017)
    mu_vec = np.array([0, 0])
    cov_mat = np.array([[1, 0], [0, 1]])
    x_2Dgauss = np.random.multivariate_normal(mu_vec, cov_mat, 10000)
    x_test = np.random.multivariate_normal(mu_vec, cov_mat, 1000)

    sigma, nlls = cross_validate_sigma(x_2Dgauss, x_test, np.linspace(1e-2, 1, 20), batch_size=100)
    fig = plt.figure(figsize=(6, 6))
    ax = fig.gca()
    ax.plot(np.linspace(1e-2, 1, 20), nlls, '-*r')
    ax.set(xlabel='log likelihood', ylabel='\sigma value', title='NLL method ')
    print(sigma)
    fig.savefig('./{}.png'.format('NLL_method'))
    plt.show()
    parzen = parzen_estimation(x_test, sigma, mode='gauss')
    ll = get_nll(x_test, parzen, batch_size=100)
    se = ll.std() / np.sqrt(x_test.shape[0])
    print("Log-Likelihood of test set = {}, se: {}".format(ll.mean(), se))


    # sigma, nll = cross_validate_sigma(x_2Dgauss, x_test, np.linspace(1e-4, 1, 20), batch_size=100)
    # fig = plt.figure(figsize=(6, 6))
    # ax = fig.gca()
    # ax.plot(np.linspace(1e-4, 1, 20), nll,  '-*r')
    # ax.set(xlabel='log likelihood', ylabel='\sigma value', title='NLL method ')
    # print(sigma)
    # pg = parzen_estimation(x_2Dgauss, sigma, mode='gauss')
    print(pdf_multivaraible_gauss(np.array([[0], [0]]), np.array([[0], [0]]), cov_mat),
          pdf_multivaraible_gauss(np.array([[1], [1]]), np.array([[0], [0]]), cov_mat))
    print(parzen_estimation(x_2Dgauss, sigma, mode='gauss')(np.array([[0, 0], [1, 1]])),
          parzen_estimation(x_2Dgauss, 0.2, mode='gauss')(np.array([[0, 0], [1, 1]])))
    # fig.savefig('./{}.png'.format('NLL_method'))
    plt.show()

    # #误差法
    # tr = pdf_multivaraible_gauss(np.array([[0], [0]]), np.array([[0], [0]]), cov_mat)
    # errs = []
    # sigmas = np.linspace(1e-4, 1, 1000)
    # for sigma in sigmas:
    #     pg = parzen_estimation(x_2Dgauss, sigma, mode='gauss')
    #     err = np.abs(pg(np.array([[0, 0]]))-tr)
    #     errs.append(err)
    # # fig = plt.figure(figsize=(6, 6))
    # # ax = fig.gca()
    # # ax.plot(sigmas, errs, '-*b')
    # # ax.set(xlabel='AE', ylabel='\sigma value', title='AE method ')
    # ind = np.argmin(errs)
    # print(sigmas[ind])
    # # fig.savefig('./{}.png'.format('AE_method'))
    # # plt.show()

def main3():
    # Make data
    mu_vec = np.array([0, 0])
    cov_mat = np.array([[1, 0], [0, 1]])
    x_2Dgauss = np.random.multivariate_normal(mu_vec, cov_mat, 10000)
    ph = parzen_estimation(x_2Dgauss, 0.2, mode='hypercube')
    pg = parzen_estimation(x_2Dgauss, 0.2, mode='gauss')
    print(pdf_multivaraible_gauss(np.array([[0], [0]]), np.array([[0], [0]]), cov_mat))
    print(pdf_multivaraible_gauss(np.array([[1], [1]]), np.array([[0], [0]]), cov_mat))
    print(ph(np.array([[0, 0], [1, 1]])))  # 输出估计的概率密度值
    print(pg(np.array([[0, 0], [1, 1]])))

def load_mnist(images_path, labels_path):
    """Load real valued MNIST data """
    with open(labels_path, 'rb') as lbpath:
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)
    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack('>IIII', imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 28, 28, 1)
    return images, labels

def get_real_valued_mnist(dataset_dir, usage = 'all',num_samples=None, num_parallel_workers=1, shuffle=None,
                          sampler=None, num_shards=None, shard_id=None):
    '''
    Load real valued MNIST dataset
    Args:
    dataset_dir (str): Path to the root directory that contains the dataset.
    usage (str, optional): Usage of this dataset, can be "train", "test" or "all" . "train" will read from 60,000
    train samples, "test" will read from 10,000 test samples, "all" will read from all 70,000 samples.
    (default=None, will read all samples)
    '''
    if usage == 'train':
        images_path = os.path.join(dataset_dir, f'train-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, f'train-labels.idx1-ubyte')
        images, labels = load_mnist(images_path, labels_path)
    elif usage == 'test':
        images_path = os.path.join(dataset_dir, f't10k-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, f't10k-labels.idx1-ubyte')
        images, labels = load_mnist(images_path, labels_path)
    elif usage == 'all':
        images_path = os.path.join(dataset_dir, 'train', f'train-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, 'train', f'train-labels.idx1-ubyte')
        images1, labels1 = load_mnist(images_path, labels_path)
        images_path = os.path.join(dataset_dir, 'test', f't10k-images.idx3-ubyte')
        labels_path = os.path.join(dataset_dir, 'test', f't10k-labels.idx1-ubyte')
        images2, labels2 = load_mnist(images_path, labels_path)
        images, labels = np.concatenate([images1, images2],axis=0), np.concatenate([labels1, labels2],axis=0)
    else:
        raise ValueError(f"Unknow usage: {usage}")

    dataset = (images, labels)

    return dataset


# 构造Parzen window估计函数
def parzen_estimation(mu, sigma, mode='gauss'):
    """
    Implementation of a parzen-window estimation
    Keyword arguments:
        x: A "nxd"-dimentional numpy array, which each sample is
                  stored in a separate row (=training example)
        mu: point x for density estimation, "dx1"-dimensional numpy array
        sigma: window width
    Return the density estimate p(x)
    """
    def log_mean_exp(a):
        # print(a.shape)
        max_ = a.max(axis=1)
        return max_ + np.log(np.exp(a - np.expand_dims(max_, axis=1)).mean(1))

    def gaussian_window(x, mu, sigma):
        # x:nxd ,mu:dx1
        # print(x.shape,mu.shape,sigma)
        a = (np.expand_dims(x, axis=1) - np.expand_dims(mu, axis=0)) / sigma
        # print(a.shape)# (100, 1000, 784)
        E = log_mean_exp(-0.5 * (a ** 2).sum(-1))
        Z = mu.shape[1] * np.log(sigma * np.sqrt(np.pi * 2))
        # print(mu.shape[-1])
        # print(np.log(sigma * np.sqrt(np.pi * 2)) > 0)
        # print((np.log(sigma)+ np.log(np.sqrt(np.pi * 2)) > 0))
        # print((E - Z) > 0)
        # return np.exp(E - Z)
        return E - Z

    def hypercube_kernel(x, mu, h):
        n, d = mu.shape

        a = (np.expand_dims(x, axis=1) - np.expand_dims(mu, axis=0)) / h
        b = np.all(np.less(np.abs(a), 1/2), axis=-1) #检查x1是否小于x2
        kn = np.sum(b.astype(int), axis=-1)
        return kn / (n * h**d) # 概率密度

    if mode is 'gauss':
        return lambda x: gaussian_window(x, mu, sigma)
    elif mode is 'hypercube':
        return lambda x: hypercube_kernel(x, mu, h=sigma)



# Pdf estimation
def pdf_multivaraible_gauss(x, mu, cov):
    # print('pdf',x.shape)
    part1 = 1 / ((2 * np.pi) ** (len(mu) / 2) * (np.linalg.det(cov) ** (1 / 2)))
    part2 = (-1 / 2) * (x - mu).T.dot(np.linalg.inv(cov)).dot((x - mu))
    # part2 = (-1 / 2) * (x - mu).dot(np.linalg.inv(cov)).dot((x - mu).T)
    # print(part2.shape)
    return part1 * np.exp(part2)


def get_nll(x, parzen, batch_size=10):
    """
    Credit: Yann N. Dauphin
    """

    inds = range(x.shape[0])
    n_batches = int(np.ceil(float(len(inds)) / batch_size))
    nlls = []
    for i in range(n_batches):
        nll = parzen(x[inds[i::n_batches]])
        nlls.extend(nll)
    return np.array(nlls)

def find_sigma(samples, data, batch_size,sigma):
    parzen = parzen_estimation(samples, sigma, mode='gauss')
    ll = get_nll(data, parzen, batch_size=batch_size)
    del parzen
    gc.collect()
    print('sigma:', sigma, 'll_m:', ll.mean())
    return ll.mean()

def cross_validate_sigma(samples, data, sigmas, batch_size, num_of_thread):

    lls = []
    p = Pool(num_of_thread)
    func = partial(find_sigma, samples,data,batch_size)
    lls = p.map(func,sigmas)

    # for sigma in sigmas:
    #     parzen = parzen_estimation(samples, sigma,mode='gauss')
    #     ll = get_nll(data, parzen, batch_size = batch_size)
    #     lls.append(ll.mean())
    #     del parzen
    #     gc.collect()
    #     print('sigma:', sigma,'ll_m:',ll.mean())
    ind = np.argmax(lls)
    return sigmas[ind],lls





if __name__ == '__main__':
    main()
    # main2()
    # main3()

