# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
######################## eval cgan example ########################
eval cgan according to model file:
python eval.py --dataset_path ./data --net_type cnn --G_ckpt ./outdir/ckpt/rank0generator1.ckpt
--D_ckpt ./outdir/ckpt/rank0discriminator10.ckpt --device_target CPU --epoch_size 10
"""

import numpy as np
from mindspore import Tensor, context
from mindspore.common import dtype as mstype
from src.cgan import get_G_and_D
import mindspore.nn as nn
import mindspore.ops.functional as F
import mindspore.ops.operations as P
from src.model_utils.config import config
from src.utils.reporter import Reporter
from src.utils.tools import load_ckpt
from src.dataset import get_real_valued_mnist
from src.parzen_numpy import cross_validate_sigma, parzen_estimation, get_nll
from mindspore.common import set_seed
set_seed(1)
"""cnn32
python eval.py --net_type cnn --G_ckpt ./outdir/ckpt/cnn64.ckpt --parzen_estimates True --cross_validate_sigma True  --generator_size 10000 
python eval.py --net_type cnn --features_d 32 --G_ckpt ./outdir/ckpt/cnn32.ckpt --D_ckpt ./outdir/ckpt/cnn32d.ckpt --parzen_estimates True --cross_validate_sigma True  --generator_size 10000 
python eval.py --net_type mlp --G_ckpt ./outdir/ckpt/mlp.ckpt --parzen_estimates True --cross_validate_sigma True  --generator_size 10000 
"""
def main():
    context.set_context(mode=context.GRAPH_MODE, device_target=config.device_target)
    G, D = get_G_and_D(config)
    load_ckpt(config, G, D)
    G.set_train(False)
    D.set_train(False)

    reporter = Reporter(config)

    reporter.info('==========start predict %s===============')
    reporter.start_predict()
    batch_size = config.batch_size
    size = config.generator_size
    steps = np.ceil(size/batch_size).astype(np.int32)
    sample_list = []
    resize = P.ResizeBilinear((28,28))
    for step in range(steps):
        noise = Tensor(np.random.normal(size=(batch_size, config.noise_dim)),
                       dtype=mstype.float32)
        label = Tensor((np.arange(noise.shape[0]) % 10), dtype=mstype.int32)
        samples = G(noise, label)  # 10000
        # print(samples.shape, samples.asnumpy().max(), samples.asnumpy().min())
        if config.net_type == 'cnn':
            samples = resize(samples)
        samples = samples.asnumpy()
        sample_list.append(samples)
        reporter.visualizer_eval(samples,samples, step)
    reporter.end_predict(step)

    if config.parzen_estimates:
        samples = np.concatenate(sample_list, axis=0)
        samples = samples[:size]
        sample_data = samples.reshape(-1, 784).astype('float')
        print(sample_data.shape, sample_data.max(), sample_data.min())

        dataset_train = get_real_valued_mnist(dataset_dir=r'./data/train', usage='train', out_as_numpy=True)
        dataset_test = get_real_valued_mnist(dataset_dir=r'./data/test', usage='test', out_as_numpy=True)
        dataset_train = dataset_train[0]
        dataset_test = dataset_test[0]
        # print(dataset.shape, dataset.dtype, type(dataset))
        valid_data = dataset_train[50000:60000].reshape(-1, 784).astype('float')
        test_data = dataset_test.reshape(-1, 784).astype('float')
        valid_data = valid_data / 255
        test_data = test_data / 255

        # for test
        # sample_data = valid_data = dataset_train[0:10000].reshape(-1, 784).astype('float')
        # print(sample_data.shape, sample_data.max(), sample_data.min())
        # sample_data = sample_data / 255
        # print(sample_data.shape, sample_data.max(), sample_data.min())

        sigma = config.sigma
        if config.cross_validate_sigma:
            sigma_range = np.logspace(-1, 0, 10)
            sigma, lls = cross_validate_sigma(sample_data, valid_data, sigma_range, batch_size, 2)
        print(sigma)

        parzen = parzen_estimation(sample_data, sigma, mode='gauss')
        ll = get_nll(test_data, parzen, batch_size=batch_size)
        se = ll.std() / np.sqrt(test_data.shape[0])
        print("Log-Likelihood of test set = {}, se: {}".format(ll.mean(), se))


if __name__ == "__main__":
    main()

